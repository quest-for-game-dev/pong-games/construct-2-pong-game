# :video_game: Construct 2 Pong Game

```text
   _____                _                   _     ___             _____
  / ____|              | |                 | |   |__ \           |  __ \
 | |     ___  _ __  ___| |_ _ __ _   _  ___| |_     ) |  ______  | |__) |__  _ __   __ _
 | |    / _ \| '_ \/ __| __| '__| | | |/ __| __|   / /  |______| |  ___/ _ \| '_ \ / _` |
 | |___| (_) | | | \__ \ |_| |  | |_| | (__| |_   / /_           | |  | (_) | | | | (_| |
  \_____\___/|_| |_|___/\__|_|   \__,_|\___|\__| |____|          |_|   \___/|_| |_|\__, |
                                                                                    __/ |
                                                                                   |___/
```

A Pong game created with Construct 2 game making software.

Pong simulates a table tennis game between two players. Each player controls a paddle either on the left or the right of the screen.  
Players prevent the other player from scoring points by preventing the ball from going past their side of the screen.

## :beginner: How to Create a Pong Game

Follow the [Quest: Pong](https://quest-for-game-dev.gitlab.io/quest-pong-tutorial/) guide on how to create Pong games using your favorite programming language.

## :clipboard: Requirements for this Project

- If you still have a Construct 2 license the project file (.caproj) can be open in the Construct 2 editor. [Construct 3 can import Construct 2 projects](https://www.construct.net/en/make-games/manuals/construct-3/tips-and-guides/importing-c2-projects).
- Litetween v1.7 for Construct 2 need to be installed || [Instructional YouTube video](https://www.youtube.com/watch?v=Lgiyg3pp5LU)

:warning: You definitely need to have a license for either Construct 2 or 3 to open this project, because this project uses features that are not available in the free version.

## :checkered_flag: Getting Started

1. Clone the repository: `git clone git@gitlab.com:quest-for-game-dev/pong-games/construct-2-pong-game.git`.
2. Start the Construct 2 game making software.
3. Browse to the cloned repository folder and open the *.caproj* file.

## :package: Build with

- [Construct 2](https://www.construct.net/en/construct-2/download) -- game making software
- [Kenney Assets](https://www.kenney.nl/assets) -- art assets

## :page_with_curl: The License

This project is licensed under the GNU GPLv3 License -- see the [LICENSE](LICENSE) file for details
